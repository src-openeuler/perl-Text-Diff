%define mod_name Text-Diff
Name:           perl-%{mod_name}
Version:        1.45
Release:        9
Summary:        Perform diffs on files and record sets
License:        (GPL-1.0-or-later OR Artistic-1.0-Perl) and (GPL-2.0-or-later OR Artistic-1.0-Perl) and MIT
URL:            https://metacpan.org/release/%{mod_name}
Source0:        https://cpan.metacpan.org/authors/id/N/NE/NEILB/%{mod_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  make
BuildRequires:  perl-interpreter
BuildRequires:  perl-generators
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(strict)
Requires:       perl(Algorithm::Diff) >= 1.19

# Remove under-specified dependencies
%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}^perl\\(Algorithm::Diff\\)$

%description
Text::Diff provides a basic set of services akin to the GNU diff utility.

%prep
%setup -q -n %{mod_name}-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*

%check
make test

%files
%license LICENSE
%{perl_vendorlib}/*

%package_help
%files help
%doc Changes README
%{_mandir}/man3/*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 1.45-9
- drop useless perl(:MODULE_COMPAT) requirement

* Mon Oct 24 2022 yangmingtai <yangmingtai@huawei.com> - 1.45-8
- define mod_name to opitomize the specfile

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.45-7
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:change mod of files

* Mon Sep 23 2019 shenyangyang<shenyangyang4@huawei.com> - 1.45-6
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise requires of perl

* Fri Aug 30 2019 hexiaowen <hexiaowen@huawei.com> - 1.45-5
- Package init
